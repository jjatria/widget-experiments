#!/usr/bin/env raku

use Test;
use Rakui::Grammar;

subtest 'Plain text' => {
    is-deeply parse-style( 'foo 🌍' ), (
        %( char => 'f',  color => 'default', width => 1 ),
        %( char => 'o',  color => 'default', width => 1 ),
        %( char => 'o',  color => 'default', width => 1 ),
        %( char => ' ',  color => 'default', width => 1 ),
        %( char => '🌍', color => 'default', width => 2 ),
    );
}

subtest 'Styled string' => {
    my $color = 'red bold underline on_blue';

    is-deeply parse-style( '[foo bar!](fg:red,bg:blue,mod:bold|underline)' ), (
        %( char => 'f', :$color, width => 1 ),
        %( char => 'o', :$color, width => 1 ),
        %( char => 'o', :$color, width => 1 ),
        %( char => ' ', :$color, width => 1 ),
        %( char => 'b', :$color, width => 1 ),
        %( char => 'a', :$color, width => 1 ),
        %( char => 'r', :$color, width => 1 ),
        %( char => '!', :$color, width => 1 ),
    );
}

subtest 'Color variants' => {
    my %color-map = (
        black     => Nil,
        red       => Nil,
        green     => Nil,
        yellow    => Nil,
        blue      => Nil,
        magenta   => Nil,
        cyan      => Nil,
        white     => Nil,
        default   => Nil,
        clear     => 'default',
        '#256'    => '256',
        '#009aff' => '0,154,255'
    );

    for %color-map.kv -> $in, $out {
        my ($cell) = parse-style( "[x](fg:$in,bg:$in)" );
        is $cell<color>, ( "$_ on_$_" given $out // $in ), $in;
    }
}

subtest 'Syntax errors' => {
    diag "TODO: Handle syntax errors";

    # Should warn
    is-deeply parse-style( '[foo](fg:bold)' ), Nil;
    is-deeply parse-style( '[foo]()' ), Nil;

    # Should parse as plain text
    is-deeply parse-style( '[foo]' ), Nil;
}

subtest 'Styled string overwriting attribute' => {
    my $color = 'blue bold';

    is-deeply parse-style( '[foo](fg:red,fg:blue,mod:bold)' ), (
        %( char => 'f', :$color, width => 1 ),
        %( char => 'o', :$color, width => 1 ),
        %( char => 'o', :$color, width => 1 ),
    );
}

done-testing;
