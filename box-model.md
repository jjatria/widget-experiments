# Our box model

A box is the most basic type of widget. All of our widgets are boxes.

A box is a widget as defined by Terminal::Print.

In general, whenever we say "widget" we'll mean a widget as defined by us
(one that is ultimately a Box). If we ever need to mean a Terminal::Print
widget we'll specify.

Because of that, all boxes have a grid they can use to draw, and
consequently, they _cannot_ draw outside of this grid.

The size of a box is, in principle, set on construction and is immutable.[^1]

[^1]: Once reflowing is implemented, this might no longer be the case.

A box has a margin and a padding.

The padding is the area inside the grid over which the Box will not
normally draw.

The margin is an area _outside_ the grid which the Box will attempt
to keep clear.

A box may have a border.

Since the box must be able to draw its border, the border _must_ lie within
the grid.

Since the box will not normally draw on top of its border, the border
must lie within the padding.

A box with no padding cannot draw a border.

The default padding of a Box is 1, which is also the width of the border.

The diagram below represents a 20 x 10 box, with a margin of
width 3 (represented by the ⨯ characters) and a padding of 2
(represented by the · characters).

                    ⨯⨯⨯⨯⨯⨯⨯⨯⨯⨯⨯⨯⨯⨯⨯⨯⨯⨯⨯⨯⨯⨯⨯⨯⨯⨯
                    ⨯⨯⨯⨯⨯⨯⨯⨯⨯⨯⨯⨯⨯⨯⨯⨯⨯⨯⨯⨯⨯⨯⨯⨯⨯⨯
                    ⨯⨯⨯⨯⨯⨯⨯⨯⨯⨯⨯⨯⨯⨯⨯⨯⨯⨯⨯⨯⨯⨯⨯⨯⨯⨯
                    ⨯⨯⨯····················⨯⨯⨯
                    ⨯⨯⨯····················⨯⨯⨯
                    ⨯⨯⨯··                ··⨯⨯⨯
                    ⨯⨯⨯··                ··⨯⨯⨯
                    ⨯⨯⨯··                ··⨯⨯⨯
                    ⨯⨯⨯··                ··⨯⨯⨯
                    ⨯⨯⨯··                ··⨯⨯⨯
                    ⨯⨯⨯··                ··⨯⨯⨯
                    ⨯⨯⨯····················⨯⨯⨯
                    ⨯⨯⨯····················⨯⨯⨯
                    ⨯⨯⨯⨯⨯⨯⨯⨯⨯⨯⨯⨯⨯⨯⨯⨯⨯⨯⨯⨯⨯⨯⨯⨯⨯⨯
                    ⨯⨯⨯⨯⨯⨯⨯⨯⨯⨯⨯⨯⨯⨯⨯⨯⨯⨯⨯⨯⨯⨯⨯⨯⨯⨯
                    ⨯⨯⨯⨯⨯⨯⨯⨯⨯⨯⨯⨯⨯⨯⨯⨯⨯⨯⨯⨯⨯⨯⨯⨯⨯⨯

If we render the border on the box drawn above, it would look like in
the below diagram (where the border has been rendered in a a double line,
with the ═ and related characters).

                   ⨯⨯⨯⨯⨯⨯⨯⨯⨯⨯⨯⨯⨯⨯⨯⨯⨯⨯⨯⨯⨯⨯⨯⨯⨯⨯
                   ⨯⨯⨯⨯⨯⨯⨯⨯⨯⨯⨯⨯⨯⨯⨯⨯⨯⨯⨯⨯⨯⨯⨯⨯⨯⨯
                   ⨯⨯⨯⨯⨯⨯⨯⨯⨯⨯⨯⨯⨯⨯⨯⨯⨯⨯⨯⨯⨯⨯⨯⨯⨯⨯
                   ⨯⨯⨯╔══════════════════╗⨯⨯⨯
                   ⨯⨯⨯║··················║⨯⨯⨯
                   ⨯⨯⨯║·                ·║⨯⨯⨯
                   ⨯⨯⨯║·                ·║⨯⨯⨯
                   ⨯⨯⨯║·                ·║⨯⨯⨯
                   ⨯⨯⨯║·                ·║⨯⨯⨯
                   ⨯⨯⨯║·                ·║⨯⨯⨯
                   ⨯⨯⨯║·                ·║⨯⨯⨯
                   ⨯⨯⨯║··················║⨯⨯⨯
                   ⨯⨯⨯╚══════════════════╝⨯⨯⨯
                   ⨯⨯⨯⨯⨯⨯⨯⨯⨯⨯⨯⨯⨯⨯⨯⨯⨯⨯⨯⨯⨯⨯⨯⨯⨯⨯
                   ⨯⨯⨯⨯⨯⨯⨯⨯⨯⨯⨯⨯⨯⨯⨯⨯⨯⨯⨯⨯⨯⨯⨯⨯⨯⨯
                   ⨯⨯⨯⨯⨯⨯⨯⨯⨯⨯⨯⨯⨯⨯⨯⨯⨯⨯⨯⨯⨯⨯⨯⨯⨯⨯

Although the box has a padding with a size of 2, only one of
these is visible, and the other is hidden by the border itself.

From this it follows that drawing a border on a box with no padding
is impossible.
