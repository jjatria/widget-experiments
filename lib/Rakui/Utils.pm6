unit package Rakui::Utils;

#| Take a list of cell hashes and wrap them into lines of a
#| maximum width. Cell hashes are expected to have a 'width'
#| field with the number of columns their characters should
#| take on the screen.
proto sub wrap-cells (|) is export {*}
multi sub wrap-cells ( Any $cells, :$width ) { [] }
multi sub wrap-cells ( @cells!, :$width = 80, :$color = 'default' ) {
    my @lines = [ [], ];
    my @word;

    my $space = { char => ' ', width => 1, :$color };
    my $line-width = 0;

    # Push the current word to the current line, or to a new one
    # if it doesn't fit
    sub word-push {
        my $word-width = [+] @word».<width>;

        # note " Line width: $line-width";
        # note " Word width: $word-width";
        # note " Max  width: $width";

        if $line-width + $word-width > $width {
            # note 'Word does not fit in line, start a new one';
            @lines.push: [ |@word, $space ];
            $line-width = [+] @lines.tail».<width>;
        }
        else {
            # note 'Word fits in line, add to current one';
            @lines.tail.push: |@word, $space;
            $line-width += 1 + $word-width;
        }

        @word = ();
    }

    for @cells -> $cell {
        # note 'Parsing ' ~ $cell<char>;
        # note "  Word is: '{ @word.map( *.<char> ).join }'";
        # note "  Line is: '{ @lines.tail.map( *.<char> ).join }'";
        # note '';

        # Part of a word
        when $cell<char> !~~ /\s/ {
            @word.push: $cell;
        }

        # Blank between words
        when !@word.elems {
            $line-width++;
            @lines.tail.push: $space;
        }

        default { word-push }

        LAST { word-push }
    }

    return @lines;
}
