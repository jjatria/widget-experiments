unit package Rakui;

grammar Grammar {
    token TOP { [ <style> || <plain> ]+ }

    token plain { <text> }

    token style { '[' <text> ']' '(' <pair>+ % ',' ')' }

    token text {
        [
        | <-[ \[ \] ]>
        | <after \\> <[ \[ \] ]>
        ]+
    }

    token pair {
        | <key=.color-key>    ':' <value=.color-value>
        | <key=.modifier-key> ':' [ <value=.modifier-value>+ % '|' ]
    }

    proto token color-key                     {*}
          token color-key:sym<fg>             {<sym>}
          token color-key:sym<bg>             {<sym>}

    proto token modifier-key                  {*}
          token modifier-key:sym<mod>         {<sym>}

    proto token color-value                   {*}
          token color-value:sym<black>        { <sym>  }
          token color-value:sym<blue>         { <sym>  }
          token color-value:sym<cyan>         { <sym>  }
          token color-value:sym<default>      { <sym>  | 'clear' }
          token color-value:sym<green>        { <sym>  }
          token color-value:sym<magenta>      { <sym>  }
          token color-value:sym<red>          { <sym>  }
          token color-value:sym<white>        { <sym>  }
          token color-value:sym<yellow>       { <sym>  }
          token color-value:sym<hex>          { '#' <xdigit> ** 6 }
          token color-value:sym<number>       { '#' <digit>  ** 3 }

    proto token modifier-value                {*}
          token modifier-value:sym<bold>      { <sym> }
          token modifier-value:sym<reset>     { <sym> | 'clear' }
          token modifier-value:sym<inverse>   { <sym> | 'reverse' }
          token modifier-value:sym<underline> { <sym> }
}

class Grammar::Actions {
    use Cache::LRU;

    has Str $.fg  = '';
    has Str $.bg  = '';
    has Str @.mod = '';

    has $!cache = Cache::LRU.new( size => 1024 );

    method !wcswidth ( $str --> Int ) {
        [+] $str.comb.map: {
            $!cache{$str}
                //= $str.uniprop                     eq 'Mn' | 'Me' ?? 0
                !!  $str.uniprop('East_Asian_Width') eq 'W'  | 'F'  ?? 2
                !!  1;
        }
    }

    method !color-string ( :$fg = $!fg, :$bg = $!bg, :@mod = @!mod ) {
        my $color = ( $fg, @mod ).flat.grep( *.so ).join: ' ';
        $color ~= ' on_' ~ $bg if $bg;
        return $color;
    }

    method TOP ($/) { make ( $/.chunks».value».made )[*;*] }

    method plain ($/) {
        make $/.Str.comb.map: -> $char {
            { :$char, color => self!color-string, width => self!wcswidth($char) };
        };
    }

    method style ($/) {
        my %style = $<pair>».made;

        my $color = self!color-string(|%style);

        make $<text>.Str.comb.map: -> $char {
            { :$char, :$color, width => self!wcswidth($char) };
        };
    }

    method pair ($/) {
        make $<key>.made => ( $<value>».made )
    }

    method text ($/) { make ~$/ }

    method color-key:sym<fg>             ($/) { make 'fg'  }
    method color-key:sym<bg>             ($/) { make 'bg'  }
    method modifier-key:sym<mod>         ($/) { make 'mod' }

    method color-value:sym<black>        ($/) { make $/ }
    method color-value:sym<blue>         ($/) { make $/ }
    method color-value:sym<cyan>         ($/) { make $/ }
    method color-value:sym<default>      ($/) { make 'default' }
    method color-value:sym<green>        ($/) { make $/ }
    method color-value:sym<magenta>      ($/) { make $/ }
    method color-value:sym<red>          ($/) { make $/ }
    method color-value:sym<white>        ($/) { make $/ }
    method color-value:sym<yellow>       ($/) { make $/ }

    method color-value:sym<number> ($/) {
        make $<digit>.join;
    }

    method color-value:sym<hex> ($/) {
        make $<xdigit>.join.comb(2).map(*.parse-base: 16).join(',');
    }

    method modifier-value:sym<bold>      ($/) { make 'bold'      }
    method modifier-value:sym<clear>     ($/) { make 'reset'     }
    method modifier-value:sym<reverse>   ($/) { make 'inverse'   }
    method modifier-value:sym<underline> ($/) { make 'underline' }
}

sub parse-style ( Str:D $text, |c ) is export {
    my $actions = Rakui::Grammar::Actions.new: |c;
    Rakui::Grammar.parse( $text, :$actions ).made;
}
