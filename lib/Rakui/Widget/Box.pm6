use Terminal::Print::Widget;
use Terminal::Print::BoxDrawing;

unit class Rakui::Widget::Box
    is   Terminal::Print::Widget
    does Terminal::Print::BoxDrawing;

has Bool $.border is rw = True;
has Str  $.title  is rw = '';

has Int $.padding-top    = 1;
has Int $.padding-bottom = 1;
has Int $.padding-left   = 1;
has Int $.padding-right  = 1;

has Str $.color;
has Str $.background;

has Int $.margin-bottom  = 0;
has Int $.margin-right   = 0;
method margin-top  () { $.y }
method margin-left () { $.x }

method color-string () {
    ( ( $!color || '' ) ~ ( ' on_' ~ $!background if $!background ) ).trim
}

# We don't want to automatically print to the grid
method TWEAK () { $.grid.disable }

method new ( :margin-top(:$y), :margin-left(:$x), |c) {
    nextwith( |c, :$x, :$y );
}

method clear () {
    given $.grid -> $grid {
        $grid.clear;
        my $color = $.color-string or return;

        my $width = $grid.w - $!padding-right;
        $grid.set-span-color( 0, $width, $_, $color ) for 0 .. $grid.h;
    }
}

method draw-border () {
    return unless
           $!padding-bottom
        && $!padding-top
        && $!padding-left
        && $!padding-right;

    my $color = $.color-string;

    self.draw-box(
        0,       0,
        $.w - 1, $.h - 1,
        |( :$color if $color ),
    );
}

method composite () {
    self.draw-border if $!border;

    self.set-string(
        2 - $!padding-left,
        0 - $!padding-top,
        $!title,
    ) if $!title;

    self.children».composite;
    callsame;
}

method change-cell ( $x, $y, %cell ) {
    $.grid.change-cell:
        $x + $!padding-left,
        $y + $!padding-top,
        %cell;
}

method set-string ( $x, $y, $string ) {
    $.grid.print-string:
        $x + $!padding-left,
        $y + $!padding-top,
        $string;
}

# Positional constructors

method left-of (
    Terminal::Print::Widget:D $ref,
    :margin-top( :$y) = 0,
    :margin-left(:$x) = 0,
    |c
) {
    self.new: |c,
        x => $ref.x - c<w> - $x - c<margin-right> || 0,
        y => $ref.y + $y;
}

method right-of (
    Terminal::Print::Widget:D $ref,
    :margin-top( :$y) = 0,
    :margin-left(:$x) = 0,
    |c
) {
    self.new: |c,
        x => $ref.x + $ref.w + $x,
        y => $ref.y + $y;
}

method above (
    Terminal::Print::Widget:D $ref,
    :margin-top( :$y) = 0,
    :margin-left(:$x) = 0,
    |c
) {
    self.new: |c,
        x => $ref.x + $x,
        y => $ref.y - c<h> - $y - c<margin-bottom>;
}

method below(
    Terminal::Print::Widget:D $ref,
    :margin-top( :$y) = 0,
    :margin-left(:$x) = 0,
    |c
) {
    self.new: |c,
        x => $ref.x + $x,
        y => $ref.y + $ref.h + $y;
}
