use Rakui::Widget::Box;

unit class Rakui::Widget::TextBox is Rakui::Widget::Box;

use Rakui::Utils;
use Rakui::Grammar;

has       @!lines;
has Str   $!text is built = '';

# We keep a cached copy of the parsed and wrapped version
# of $!text in @!lines. In order to invalidate this cache,
# we hook into assignments to it with a custom Proxy object
method text () is rw {
    my $text  := $!text;
    my @lines := @!lines;

    Proxy.new(
        FETCH => method () { $text },
        STORE => method ($value) { $text = $value; @lines = () },
    );
}

method composite () {
    # If the cached lines is not valid (either because $!text has
    # changed or because it was never calculated in the first place)
    # clear the grid and parse / wrap $!text, and cache the result
    unless @!lines {
        self.clear;
        my $width = $.w - $.padding-left - $.padding-right;

        my $fg    = $.color;
        my $bg    = $.background;
        my $color = $.color-string;

        @!lines = $!text.split(rx/\n ** 2..*/).grep(?*).map: -> $para {
            |(
                |$para.&parse-style( :$fg, :$bg ).&wrap-cells(:$width, :$color),
                [], # Add an extra empty line between paragraphs
            );
        }
    }

    # Copy the cells and their styles in the grid
    my $effective-height = $.h
        - $.padding-top
        - $.padding-bottom;

    for @!lines.kv -> $y, @line {
        next unless @line.elems;

        last if $y >= $effective-height;

        my $shift = 0;
        for @line.kv -> $x, %cell {
            next unless %cell<width>;

            self.change-cell( $x + $shift, $y, %cell );

            # We're done if this character is not wide
            next unless %cell<width> > 1;

            for 1 .. %cell<width> - 1 -> $mod {
                self.change-cell( $x + $mod, $y, { char => '' } );
            }

            $shift += %cell<width> - 1;
        }
    }

    callsame;
}
